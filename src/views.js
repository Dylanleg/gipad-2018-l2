import models from "./models";

function renderForm(state, actions) {
  const root = document.createElement("div");

  // Block title:
  const h3 = document.createElement("h3");
  h3.innerHTML = "Query:";
  root.appendChild(h3);

  // Fields selection:
  const fieldsSelect = document.createElement("select");
  const selectedValue = state.query.selectedField;
  state.meta.fields.forEach(function(field) {
    const option = document.createElement("option");
    option.innerHTML = field;
    option.value = field;
    if (field === selectedValue) option.selected = true;
    fieldsSelect.appendChild(option);
  });
  fieldsSelect.addEventListener("change", function(e) {
    const newValue = e.target.value;
    actions.setQueryField({selectedField: newValue});
  });
  root.appendChild(fieldsSelect);

  // Display mode selection:
  const modeSelect = document.createElement("select");
  const selectedMode = state.query.display;
  ["top5", "top10"].forEach(function(mode) {
    const option = document.createElement("option");
    option.innerHTML = mode;
    option.value = mode;
    if (mode === selectedMode) option.selected = true;
    modeSelect.appendChild(option);
  });
  modeSelect.addEventListener("change", function(e) {
    const newValue = e.target.value;
    actions.setDisplay({display: newValue});
  });
  root.appendChild(modeSelect);

  return root;
}

function renderResult(state, actions) {
  const root = document.createElement("div");

  // Block title:
  const h3 = document.createElement("h3");
  h3.innerHTML = "Result:";
  root.appendChild(h3);

  // Results list:
  const mode = state.query.display;
  const model = models[mode];
  const list = model(state.data, state.query.selectedField);
  const domList = document.createElement("ul");
  for (let i = 0; i < list.length; i++) {
    let element = list[i];
    let domElement = document.createElement("li");

    domElement.innerHTML = element.count + " - " + element.value;
    domList.appendChild(domElement);
  }
  root.appendChild(domList);

  return root;
}

export default function render(state, actions) {
  const root = document.createElement("div");

  root.appendChild(renderForm(state, actions));
  root.appendChild(renderResult(state, actions));

  return root;
}
