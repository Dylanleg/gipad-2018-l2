To make the project work:
  - Install Node.js and NPM
  - Clone the repository
  - Go into the repository from a command line
  - Type `npm install` to install dependencies
  - Then type `npm run build` to run it
